# Description

Surligneur est un Web Extension développée pour Mozilla Firefox. Elle permet de simplement surligner des passages d'une page web visitée. Ces passages sont automatiquement sauvegardés et restaurés à la prochaine visite.

# Fonctionnalités

* Surligne le passage sélectionné en appuyant sur la touche h
* Supprime le surlignage dans le passage sélectionné en appuyant sur la touche r
* Quatre couleurs disponibles
* Bouton pour effacer tout surlignage de la page
* Sauvegarde et restauration automatique des passages surlignés à la prochaine visite.

# Installation

Via la [page d'installation du module](https://addons.mozilla.org/en-US/firefox/addon/surligneur/) sur Firefox Add-ons

# Crédits

Cette extension est inspirée de [Highlightor](https://addons.mozilla.org/firefox/addon/highlightor/) et utilise la librairie [TextHighlighter](https://mir3z.github.io/texthighlighter/).
