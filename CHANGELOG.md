## Version 1.1

- Add the possibility to change the color by default at startup by double clicking on it.
- The current color is now shared between the tabs (ie. if the user changes the current highlighting color on a tab, it changes in all tabs)
- The current color is directly displayed on the addon's icon